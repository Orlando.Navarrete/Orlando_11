package ni.edu.uni.programacion1.data;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import ni.edu.uni.programacion1.pojo.Employee;

public class EmployeeData {
    private Employee[] employees;

    public EmployeeData() throws FileNotFoundException {
        populateEmployees();
    }

    public Employee[] getEmployees() {
        return employees;
    }
    
    private void populateEmployees() throws FileNotFoundException {
        Gson gson= new Gson();
        employees= gson.fromJson(new FileReader("employee_data.json"), Employee[].class);
    }
    
    public Employee[] getEmployeesBySalaryRange(float min, float max) {
        //El rango estára entre 6000 - 10000
        Employee[] employeesBySalary= null;
        for(Employee e: employees) {
            if(e.getSalary() >= min && e.getSalary() <= max) {
                employeesBySalary= addCadenaEmployee(employeesBySalary, e);
            }
        }
        return null;
    }
    
    public static Employee[] addCadenaEmployee(Employee array[], Employee value) {
        if(array == null) {
            array= new Employee[1];
            array[array.length - 1]= value;
            return array;
        }
        
        Employee tmp[]= Arrays.copyOf(array, array.length + 1);
        tmp[tmp.length - 1]= value;
        
        return array;
    }
}
