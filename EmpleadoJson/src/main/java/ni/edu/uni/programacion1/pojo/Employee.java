package ni.edu.uni.programacion1.pojo;

public class Employee {
    private int id;
    private String name;
    private String lastName;
    private String address;
    private String phone;
    private float salary;

    public Employee() {
    }

    public Employee(int id, String name, String lastName, String address, String phone, float salary) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.address = address;
        this.phone = phone;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastNama) {
        this.lastName = lastNama;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", lastNama=" + lastName + ", address=" + address + ", phone=" + phone + ", salary=" + salary + '}';
    }
}
