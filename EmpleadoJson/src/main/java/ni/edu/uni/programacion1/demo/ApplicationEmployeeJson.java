package ni.edu.uni.programacion1.demo;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import ni.edu.uni.programacion1.data.EmployeeData;
import ni.edu.uni.programacion1.pojo.Employee;

public class ApplicationEmployeeJson {

    public static void main(String[] args) {
        try {
            EmployeeData eData= new EmployeeData();
            for(Employee e: eData.getEmployeesBySalaryRange(6000, 10000)) {
                System.out.println(e.toString());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ApplicationEmployeeJson.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
