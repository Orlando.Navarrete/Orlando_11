package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import static java.lang.Thread.sleep;
import java.util.Arrays;
import ni.edu.uni.programacion1.data.DepartamentoData;
import ni.edu.uni.programacion1.data.EmpleadoData;
import ni.edu.uni.programacion1.data.MunicipioData;
import ni.edu.uni.programacion1.enums.NivelAcademico;
import ni.edu.uni.programacion1.enums.Sexo;
import ni.edu.uni.programacion1.pojo.Departamento;
import ni.edu.uni.programacion1.pojo.Empleado;
import ni.edu.uni.programacion1.pojo.Municipio;

public class ConsoleGestionEmpleado {
    public static void agregarEmpleado(BufferedReader reader, EmpleadoData edata) throws IOException {
        DepartamentoData dData= new DepartamentoData();
        MunicipioData mData= new MunicipioData();
        int codigo= ConsoleReader.readInt(reader, "Codigo: ");
        String cedula= ConsoleReader.readString(reader, "Cedula: ");
        String name1= ConsoleReader.readLetter(reader, "Primer Nombre: ");
        String name2= ConsoleReader.readLetter(reader, "Segundo Nombre: ");
        String lastname1= ConsoleReader.readLetter(reader, "Primer Apelido: ");
        String lastname2= ConsoleReader.readLetter(reader, "Segundo Nombre: ");
        String address= ConsoleReader.readString(reader, "Dirrecion: ");
        String phoneNumber= ConsoleReader.readString(reader, "Telefono: ");
        String mobileNumber= ConsoleReader.readString(reader, "Celular: ");
        int sexoId;
        do {
            sexoId= ConsoleReader.readInt(reader, "1. Femenino.\n2. Masculino.");
        }while(sexoId <= 0 || sexoId > 2);
        Sexo sexo= Sexo.values()[sexoId - 1];
        int nivelId;
        do {
            nivelId= ConsoleReader.readInt(reader, "1. Primaria.\n2. Tecnico Medio.\n3. Bachiller.\n"
                    + "4. Tecnico Superior.\n5. Licenciado.\n6. Ingeniero.\n"
                    + "7. Post Grado.\n8. Especialista.\n9. Maestria.\n10. Doctorado.\n"
                    + "11. Post Doctorado.\nOpcion: ");
        }while(nivelId <= 0 || nivelId > 10);
        NivelAcademico na= NivelAcademico.values()[nivelId - 1];
        Departamento[] departamentos= dData.getDepartamento();
        String[] nombresDepartamentos= new String[17];
        int i = 0;
        for(Departamento d: departamentos) {
            nombresDepartamentos[i++]= d.getNombre();
        }
        int dId;
        do {
            dId= ConsoleReader.readInt(reader, "Digite entre 1 - 17"
                + "para seleccionar un departamento\n"+Arrays.toString(nombresDepartamentos)); 
        }while(dId <= 0 || dId > 17);
        Departamento d= dData.getDepartamentoById(dId);
        Municipio[] municipios= mData.getMunicipioByDepartamento(d);
        String[] nombreMunicipios= new String[153];
        int j= 0;
        for(Municipio m: municipios) {
            nombreMunicipios[j++]= m.getNombre();
        }
        int mId;
        do {
            mId= ConsoleReader.readInt(reader, "Digite entre 1 - 153"
                    + "para seleccionar un municipio\n"+Arrays.toString(nombreMunicipios));
        }while(mId <= 0 || mId > 153);
        Municipio m= mData.getMunicipioById(mId);
        Empleado e = new Empleado(codigo, cedula, phoneNumber, phoneNumber, phoneNumber, lastname2, address, name2, cedula, sexo, na,municipios[0]);
        edata.add(e);
    }
    
    public static void editarEmpleado(BufferedReader reader, EmpleadoData eData) throws IOException, InterruptedException {
        int id= ConsoleReader.readInt(reader, "Id empleado: ");
        int opc;
        Empleado e= eData.getEmpleadoByCodigo(id);
        boolean flag= true;
        do {
            ConsoleMenus.subMenuGestionSubMenu();
            opc= ConsoleReader.readInt(reader, "Opcion: ");
            switch(opc) {
                case 1:
                    String name1= ConsoleReader.readString(reader, "Update first name: ");
                    e.setPrimerNombre(name1);
                    break;
                case 2:
                    String name2= ConsoleReader.readString(reader, "Update second name: ");
                    e.setPrimerNombre(name2);
                    break;
                case 3:
                    String lastName1= ConsoleReader.readString(reader, "Update first last name: ");
                    e.setPrimerApellido(lastName1);
                    break;
                case 4:
                    String lastName2= ConsoleReader.readString(reader, "Update second last name: ");
                    e.setSegundoApellido(lastName2);
                    break;
                case 5:
                    String cedula= ConsoleReader.readString(reader, "Update identification card: ");
                    e.setCedula(cedula);
                    break;
                case 6:
                    String adress= ConsoleReader.readString(reader, "Update adress: ");
                    e.setDireccion(adress);
                    break;
                case 7:
                    String telephone= ConsoleReader.readString(reader, "Update telephone: ");
                    e.setTelefono(telephone);
                    break;
                case 8:
                    String mobileTelephone= ConsoleReader.readString(reader, "Update mobile: ");
                    e.setCelular(mobileTelephone);
                    break;
                case 9:
                    MunicipioData mData= new MunicipioData();
                    Municipio[] mMunicipios= new Municipio[153];
                    for(int i = 0; i < mMunicipios.length; i++) {
                        mMunicipios[i]= mData.getMunicipioById(i);
                    }
                    String municipios[]= new String[153];
                    int j= 0;
                    for(Municipio m: mMunicipios) {
                        municipios[j]= m.getNombre();
                        j++;
                    }
                    int mId;
                    do {
                        mId= ConsoleReader.readInt(reader, "Digite entr 1 - 153"
                                + "para seleccionar un municipio: \n"+Arrays.toString(municipios));
                    }while(mId <= 0 || mId > 153);
                    Municipio m= mData.getMunicipioById(mId);
                    e.setMunicipio(m);
                    break;
                case 11:
                    System.out.println("Regresando...");
                    flag= false;
                    sleep(2000);
                    break;
                default: System.err.println("Opcion incorrecta...");
            }
        }while(flag);
    }
    
    public static void eliminarEmpleado() {
        
    }
}
