package ni.edu.uni.programacion1.demo;

import java.io.IOException;
import ni.edu.uni.programacion1.console.ConsoleDemo;

public class ApplicationDemo {

    public static void main(String[] args) throws IOException, InterruptedException {
        ConsoleDemo.start();
    }
    
}
