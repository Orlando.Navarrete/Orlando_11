package ni.edu.uni.programacion1.console;

public class ConsoleMenus {
    public static void mainMenu() {
        System.out.println("Bienvenido a su aplecacion");
        System.out.println("Seleccione una opcion");
        System.out.println("1. Gestion de Empleados");
        System.out.println("2. Reportes de Empleados");
        System.out.println("3. Salir");
    }
    
    public static void gestionSubMenu() {
        System.out.println("1. Agregar");
        System.out.println("2. Editar");
        System.out.println("3. Eliminar");
        System.out.println("4. Regresar");
    }
    
    public static void repostesSubmenu() {
        System.out.println("1. Visualizar todos");
        System.out.println("2. Visualizar por codigo");
        System.out.println("3. Visualizar por sexo");
        System.out.println("4. Visualizar por Municipio");
        System.out.println("5. Visualizar por nombre");
        System.out.println("6. Visualizar po apellido");
        System.out.println("7. Visualizar por cedula");
        System.out.println("8. Regresar");
    }
    
    public static void subMenuGestionSubMenu() {
        System.out.println("1. Editar primer nombre");
        System.out.println("2. Editar segundo nombre");
        System.out.println("3. Editar primer apellido");
        System.out.println("4. Editar segundo apellido");
        System.out.println("5. Editar cedula");
        System.out.println("6. Editar direccion");
        System.out.println("7. Editar telefono");
        System.out.println("8. Editar celular");
        System.out.println("9. Editar nivel academico");
        System.out.println("10. Editar municipio");
        System.out.println("11. Regresar");
    }
}
