package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import static java.lang.Thread.sleep;
import ni.edu.uni.programacion1.data.EmpleadoData;

public class ConsoleDemo {
    public static void start() throws IOException, InterruptedException {
        BufferedReader reader= new BufferedReader(new InputStreamReader(System.in));
        int opc= 0;
        boolean flag= true;
        
        do {
            ConsoleMenus.mainMenu();
            opc= ConsoleReader.readInt(reader, "Opcion: ");
            switch(opc) {
                case 1:
                    gestionEmpleado(reader);
                    break;
                case 2:
                    reporteEmpleado(reader);
                    break;
                case 3:
                    flag= false;
                    System.out.println("Hasta la vista...");
                    sleep(2000);
                    break;
                default: System.err.println("Opcion no valida");
            }
        }while(flag);
    }
    
    public static void gestionEmpleado(BufferedReader reader) throws IOException, InterruptedException {
        int opc;
        boolean flag = true;
        EmpleadoData edata = new EmpleadoData();
        do{
            ConsoleMenus.gestionSubMenu();
            opc = ConsoleReader.readInt(reader, "Opcion: ");
            switch(opc){
                case 1:
                    ConsoleGestionEmpleado.agregarEmpleado(reader, edata);
                    break;
                case 2:
                    ConsoleGestionEmpleado.editarEmpleado(reader, edata);
                case 3:
                    ConsoleGestionEmpleado.eliminarEmpleado();
                case 4:
                    System.out.println("Regresando...");
                    sleep(2000);
                    flag= false;
                default: System.err.println("Opcion incorrecta...");
            }
        }while(flag);

    }
    
    public static void reporteEmpleado(BufferedReader reader) throws IOException, InterruptedException {
        int opc;
        boolean flag= true;
        EmpleadoData eData= new EmpleadoData();
        do {
            ConsoleMenus.repostesSubmenu();
            opc= ConsoleReader.readInt(reader, "Opcion: ");
            switch(opc) {
                case 1:
                    ConsoleReporteEmpleado.visualizarTodos();
                    break;
                case 2:
                    ConsoleReporteEmpleado.visualizarByCodigo(reader);
                    break;
                case 3:
                    ConsoleReporteEmpleado.visualizarBySexo(reader);
                    break;
                case 4:
                    ConsoleReporteEmpleado.visualizarByMunicipio(reader);
                    break;
                case 5:
                    ConsoleReporteEmpleado.visualizarByName(reader);
                    break;
                case 6:
                    ConsoleReporteEmpleado.visualizarByLastName(reader);
                    break;
                case 7:
                    ConsoleReporteEmpleado.visualizarByCedula(reader);
                    break;
                case 8:
                    flag= false;
                    System.out.println("Regresando...");
                    sleep(2000);
                    break;
                default: System.err.println("Opcion incorrecta...");
            }
        }while(flag);
    }
}
