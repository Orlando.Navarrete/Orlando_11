package ni.edu.uni.programacion1.console;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import ni.edu.uni.programacion1.data.EmpleadoData;
import ni.edu.uni.programacion1.data.MunicipioData;
import ni.edu.uni.programacion1.enums.Sexo;
import ni.edu.uni.programacion1.pojo.Empleado;
import ni.edu.uni.programacion1.pojo.Municipio;

public class ConsoleReporteEmpleado {
    public static void visualizarTodos() {
        EmpleadoData eData= new EmpleadoData();
        Empleado[] empleados= eData.getEmpleados();
        for(Empleado e: empleados) {
            e.toString();
        }
    }
    
    public static void visualizarByCodigo(BufferedReader reader) throws IOException {
        int id= ConsoleReader.readInt(reader, "Id empleado: ");
        EmpleadoData eData= new EmpleadoData();
        Empleado empleado= eData.getEmpleadoByCodigo(id);
        empleado.toString();
    }
    
    public static void visualizarBySexo(BufferedReader reader) throws IOException {
        EmpleadoData eData= new EmpleadoData();
        int sexoId;
        do {
            sexoId= ConsoleReader.readInt(reader, "1. Femenino.\n2. Masculino.");
        }while(sexoId <= 0 || sexoId > 2);
        Sexo sexo= Sexo.values()[sexoId - 1];
        Empleado[] empleadosSexo= eData.getEmpleadosBySexo(sexo);
        for(Empleado e: empleadosSexo) {
            e.toString();
        }
    }
    
    public static void visualizarByMunicipio(BufferedReader reader) throws IOException {
        EmpleadoData eData= new EmpleadoData();
        MunicipioData mData= new MunicipioData();
        Municipio[] municipios= new Municipio[153];
        for(int i = 0; i < municipios.length; i++) {
            municipios[i]= mData.getMunicipioById(i);
        }
        String nombresMunicipios[];
        nombresMunicipios = new String[153];
        int j= 0;
        for(Municipio m: municipios) {
            nombresMunicipios[j]= m.getNombre();
            j++;
        }
        int mId;
        do {
            mId= ConsoleReader.readInt(reader, "Digite entr 1 - 153"
                + "para seleccionar un municipio: \n"+Arrays.toString(municipios));
        }while(mId <= 0 || mId > 153);
        Municipio m= mData.getMunicipioById(mId);
        Empleado[] empleadosMunicipios;
        empleadosMunicipios = eData.getEmpleadoByMunicipio(m);
    }
    
    public static void visualizarByName(BufferedReader reader) throws IOException {
        EmpleadoData eData= new EmpleadoData();
        String name= ConsoleReader.readString(reader, "Ingrese nombre: ");
        Empleado empleado= eData.getEmpleadoByName(name);
        empleado.toString();
    }
    
    public static void visualizarByLastName(BufferedReader reader) throws IOException {
        EmpleadoData eData= new EmpleadoData();
        String lastName= ConsoleReader.readString(reader, "Ingrese nombre: ");
        Empleado empleado= eData.getEmpleadoByLastName(lastName);
        empleado.toString();
    }
    
    public static void visualizarByCedula(BufferedReader reader) throws IOException {
        EmpleadoData eData= new EmpleadoData();
        String cedula= ConsoleReader.readString(reader, "Ingrese No. cedula: ");
        Empleado empleado= eData.getEmpleadoByCedula(cedula);
        empleado.toString();
    }
}
