/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.data;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import ni.edu.uni.programacion1.enums.Sexo;
import ni.edu.uni.programacion1.pojo.Empleado;
import ni.edu.uni.programacion1.pojo.Municipio;

/**
 *
 * @author DocenteFCyS
 */
public class EmpleadoData {

    private Empleado[] empleados;
    private MunicipioData mData;

    public EmpleadoData() {
        mData = new MunicipioData();
        try {
            populateEmpleado();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmpleadoData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void add(Empleado e) {
        empleados = addEmpleado(empleados, e);
    }

    public Empleado[] getEmpleados() {
        return this.empleados;
    }

    public Empleado getEmpleadoByCodigo(int codigo) {
        if (codigo <= 0) {
            return null;
        }
        Empleado e = new Empleado();
        e.setCodigo(codigo);
        Comparator<Empleado> c = new Comparator<Empleado>() {
            @Override
            public int compare(Empleado arg0, Empleado arg1) {
                return (arg0.getCodigo() - arg1.getCodigo());
            }
        };
        Arrays.sort(empleados, c);
        int index = Arrays.binarySearch(empleados, e, c);

        return empleados[index];
    }
    
    public Empleado[] getEmpleadoByMunicipio(Municipio municipio) {
        List<Empleado> list= Arrays.asList(empleados).stream().filter(e -> e.getMunicipio() == municipio).collect(Collectors.toList());
        return (Empleado[])list.toArray();
    }
    
    public Empleado getEmpleadoByCedula(String cedula){
        if(cedula == null){
            return null;
        }
        if(cedula.isEmpty() || cedula.isEmpty()){
            return null;
        }
        Empleado e = new Empleado();
        e.setCedula(cedula);
        Arrays.sort(empleados);
        int index = Arrays.binarySearch(empleados, e);
        if(index < 0){
            return null;
        }
        
        return empleados[index];
    }
    
    public Empleado getEmpleadoByName(String name) {
        int list= Arrays.binarySearch(empleados, name);
        return empleados[list];
    }
    
    public Empleado getEmpleadoByLastName(String lastName) {
        int list= Arrays.binarySearch(empleados, lastName);
        return empleados[list];
    }
    
    public Empleado[] getEmpleadosBySexo(Sexo sexo){
        List<Empleado> list = Arrays.asList(empleados).stream().filter(e -> e.getSexo() == sexo).collect(Collectors.toList());
        return (Empleado[])list.toArray();
    }

    private Empleado[] addEmpleado(Empleado[] eCopy, Empleado e) {
        if (eCopy == null) {
            eCopy = new Empleado[1];
            eCopy[eCopy.length - 1] = e;
            return eCopy;
        }

        eCopy = Arrays.copyOf(eCopy, eCopy.length + 1);
        eCopy[eCopy.length - 1] = e;

        return eCopy;
    }

    private static class EmpleadoSingleton {
        public static final EmpleadoData edata= new EmpleadoData();
    }
    
    private void populateEmpleado() throws FileNotFoundException {
        Gson gson= new Gson();
        empleados= gson.fromJson(new FileReader("empleado_data.json"), Empleado[].class);
    }
    
    public static EmpleadoData getInstance() {
        return EmpleadoSingleton.edata;
    }
}
