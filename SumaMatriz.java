import java.util.Scanner;

public class SumaMatriz {
    public static void main(String[] args) {
        /*
        Realizar un programa en JAVA que implemente un método que reciba como
        parámetro una matriz de números enteros y retorne la sumatoria total de
        todos los valores.
        Crear un arreglo por suma de filas y por suma de columnas.
        */
        //Scanner entrada= new Scanner(System.in);
        instanciarMatriz();
    }

    public static void instanciarMatriz() {
        Scanner entradaInstanciar= new Scanner(System.in);
        int f, c;
        System.out.println("Ingrese la cantidad de filas y columnas de la matriz");
        f= entradaInstanciar.nextInt(); c= entradaInstanciar.nextInt();
        int matriz[][]= new int[f][c];
        for(int i = 0; i < matriz.length; i++) {
            for(int j = 0; j < matriz[i].length; j++) {
                System.out.println("Posicion ["+i+"]["+j+"]");
                System.out.println("Ingrese un numero entero: ");
                matriz[i][j]= entradaInstanciar.nextInt();
            }
        }
        System.out.println("Matriz"); System.out.println();
        printMatriz(matriz); System.out.println();
        int matrizSuma= sumarMatriz(matriz);
        System.out.println("La suma de la matriz es: "+matrizSuma);
        System.out.println();
        int sumaFila[]= sumarFilas(matriz);
        for(int i = 0; i < sumaFila.length; i++) {
            System.out.println("La suma de la fila("+(i+1)+") es: "+sumaFila[i]);
        }
        System.out.println();
        int sumaColumna[]= sumarColumnas(matriz);
        for(int i = 0; i < sumaColumna.length; i++) {
            System.out.println("La suma de la columna("+(i+1)+") es: "+sumaColumna[i]);
        }
    } 

    public static void printMatriz(int array[][]) {
        for(int i = 0; i < array.length; i++) {
            System.out.println(java.util.Arrays.toString(array[i]));
        }
    }

    public static int sumarMatriz(int array[][]) {
        int suma= 0;
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[i].length; j++) {
                suma += array[i][j];
            }
        }
        return suma;
    }

    public static int[] sumarFilas(int array[][]) {
        int sumaFila[]= new int[array.length];
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[i].length; j++) {
                sumaFila[i] += array[i][j];
            }
        }
        return sumaFila;
    }

    public static int[] sumarColumnas(int array[][]) {
        int sumaFila[]= new int[array.length];
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[i].length; j++) {
                sumaFila[i] += array[j][i];
            }
        }
        return sumaFila;
    }
}