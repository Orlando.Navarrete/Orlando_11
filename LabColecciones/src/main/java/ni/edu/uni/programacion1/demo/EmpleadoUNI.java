package ni.edu.uni.programacion1.demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class EmpleadoUNI {

    public static void main(String[] args) {
        Scanner entrada= new Scanner(System.in);
        Random rand= new Random();
        List<String> listString= new ArrayList<>();
        System.out.print("Ingrese la cantidad de empleados: ");
        int n= entrada.nextInt();
        for (int i = 0; i < n; i++) {
            entrada.nextLine();
            System.out.println("Posicion["+(i+1)+"].");
            System.out.print("Ingrese un nombre: ");
            String name= entrada.nextLine();
            listString.add(name);
        }
        String randomElement= obtenerStringRandom(listString);
        System.out.println("El elemto es: "+randomElement);
    }
    
    public static String obtenerStringRandom(Object stringList) {
        Random rand = new Random();
        List<String> givenList = (List<String>) stringList;

        int numberOfElements = 2;
        String randomElement= "";

        for (int i = 0; i < numberOfElements; i++) {
            int randomIndex = rand.nextInt(givenList.size());
            randomElement = givenList.get(randomIndex);
        }
        
        return randomElement;
    }
}
