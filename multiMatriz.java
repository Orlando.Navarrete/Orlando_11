import java.util.*;

public class multiMatriz {
    public static void main(String[] args) {
        Scanner entrada= new Scanner(System.in);
        int n[]= new int[2], m[]= new int[2];
        /*
        n[0] es la cantidad de filas de la primera matriz
        n[1] es la cantidad de filas de la segunda matriz
        m[0] es la cantidad de columnas de la primera matriz
        m[1] es la cantidad de columnas de la segunda matriz
        */
        do {
            for(int i = 0; i < n.length; i++) {
                System.out.println("Ingrese la cantidad de filas de la matriz ["+(i+1)+"]");
                n[i]= entrada.nextInt();
                System.out.println("Ingrese la cantidad de columnas de la matriz ["+(i+1)+"]");
                m[i]= entrada.nextInt();
            }
            if(m[0] != n[1]) {//Si el if es verdadero las columnas de la matriz [1] es diferente de las filas de la matriz [2]
                System.out.println("La cantidad de columnas de la matriz [1] debe ser igual");
                System.out.println("a la cantidad de filas de la matriz[2]");
            }
        }while(m[0] != n[1]);//El while se deja de cumplir hasta que las columnas de matriz[1] es igual a las filas de matriz[2]
        int matrizA[][]= new int[n[0]][m[0]], matrizB[][]= new int[n[1]][m[1]];//Instanciamos las matrices con los valores ingresados por el usuario
        System.out.println("***Rellenando Matriz [1]***");
        for(int i = 0; i < matrizA.length; i++) {
            for(int j = 0; j < matrizA[i].length; j++) {
                System.out.println("Posicion ["+i+"]["+j+"]");
                System.out.println("Ingrese un numero entero: ");
                matrizA[i][j]= entrada.nextInt();
            }
        }
        System.out.println(); System.out.println("***Rellenando Matriz [2]***");
        for(int i = 0; i < matrizB.length; i++) {
            for(int j = 0; j < matrizB[i].length; j++) {
                System.out.println("Posicion ["+i+"]["+j+"]");
                System.out.println("Ingrese un numero entero: ");
                matrizB[i][j]= entrada.nextInt();
            }
        }
        System.out.println();
        int matrizMulti[][]= new int[n[0]][m[1]];
        for(int i = 0; i< matrizMulti.length; i++) {
            for(int j = 0; j < matrizMulti[i].length; j++) {
                for(int k = 0; k < m[0]; k++) {
                    /*
                    Este for recorre las columnas de la matriz[1] y como sabemos que las filas de la matriz[2]
                    es el mismo numero, es decir m[0] == n[1], simplemete tomamos como parametro una de ellas.
                    */
                    matrizMulti[i][j] += matrizA[i][k] * matrizB[k][j];
                }
            }
        }
        System.out.println("Matriz [1]");
        printByRow(matrizA); System.out.println();
        System.out.println("Matriz [2]");
        printByRow(matrizB); System.out.println();
        System.out.print("Matriz resultante\t"); System.out.println("Multiplicacion de matrices");
        printByRow(matrizMulti);
    }

    public static void printByRow(int matrix[][]) {
        for(int i = 0; i < matrix.length; i++) {
            System.out.println(Arrays.toString(matrix[i]));
        }
    }
}