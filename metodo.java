import java.util.Scanner;
import java.util.Arrays;

public class metodo {
    public static void main(final String[] args) {
        final int numeros[] = { 4, 5, 2, 76, 3, 6 };
		int sumaA, sumaB;
        final int arrayMatriz[][]=
        {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
        };
        sumaA= suma(numeros);
        sumaB= suma(arrayMatriz);
        printArray(numeros);
        printByRow(arrayMatriz);
        System.out.println("Suma= "+sumaA);
        System.out.println("Suma matriz= "+sumaB);
    }

    public static int suma(final int input[]) {
        int suma= 0;
        for(int a : input) {
            suma += a;
        }
        return suma;
    }

    public static int suma(int matriz[][]) {
        int suma= 0;
        for(int i = 0; i < matriz.length; i++) {
            suma += suma(matriz[i]);
        }
        return suma;
    }

    public static void printByRow(int array[][]) {
        for(int i = 0; i < array.length; i++) {
            System.out.println(java.util.Arrays.toString(array[i]));
        }
    }

    public static void printArray(int array[]) {
        System.out.println(java.util.Arrays.toString(array));
    }
    
    public static int suma(int a, int b) {
        return (a+b);
    }

}