package uni.herencia1m2.pojo;

public class Empleado extends Persona{
    private int codigo;
    private String cargo;

    public Empleado(int codigo, String cargo, String nombres, String apellido) {
        super(nombres, apellido);
        this.codigo = codigo;
        this.cargo = cargo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Override
    public String toString() {
        return "Empleado{" + "codigo:" + this.codigo + ", cargo:" + this.cargo + ", nombre: "+ super.nombres +" apellido: "+ super.apellido +'}';
    }
    
}