package uni.herencia1m2.pojo;

public abstract class Persona implements Comparable<Persona>{
    protected String nombres;
    protected String apellido;

    public Persona(String nombres, String apellido) {
        this.nombres = nombres;
        this.apellido = apellido;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Override
    public String toString() {
        return "Persona{" + "nombres=" + nombres + ", apellido=" + apellido + '}';
    }

    @Override
    public int compareTo(Persona p) {
        return this.apellido.compareToIgnoreCase(p.getApellido());
    }
    
}
