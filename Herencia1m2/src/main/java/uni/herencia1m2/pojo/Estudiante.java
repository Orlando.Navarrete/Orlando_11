package uni.herencia1m2.pojo;

public class Estudiante extends Persona{
    private String carnet;

    public Estudiante(String carnet, String nombres, String apellido) {
        super(nombres, apellido);
        this.carnet = carnet;
    }

    public String getCarnet() {
        return carnet;
    }

    public void setCarnet(String carnet) {
        this.carnet = carnet;
    }

    @Override
    public String toString() {
        return "Estudiante{" + "carnet=" + carnet + ", nombre: "+ super.nombres +" apellido: "+ super.apellido +'}';
    }
    
}
