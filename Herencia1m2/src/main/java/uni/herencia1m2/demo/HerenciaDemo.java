package uni.herencia1m2.demo;

import uni.herencia1m2.pojo.Empleado;
import uni.herencia1m2.pojo.Estudiante;
import uni.herencia1m2.pojo.Persona;

public class HerenciaDemo {

    public static void main(String[] args) {
        Persona[] personas= {
            new Empleado(1, "Marketing Digital", "Pepito", "Perez"),
            new Estudiante("2019- 0784U", "Orlando", "Navarrete"),
            new Estudiante("2019- 0790U", "Malikha", "Artola"),
            new Empleado(2, "Programadora", "Glenda", "Barrios")
        };
        for(Persona p: personas) {
            if(p.compareTo(new Estudiante("", "", "Perez")) == 0) {
                printObject(p);
            }            
        }
    }
    
    public static void printObject(Persona p) {
        System.out.println(p.toString());
    }
}
