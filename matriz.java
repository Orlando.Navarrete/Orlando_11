import java.util.Scanner;

public class matriz {
    public static void main(String[] args) {
        /*
        Realizar un programa en JAVA que implemente un método que reciba como
        parámetro una matriz de números enteros y retorne la sumatoria total de
        todos los valores.
        */ int fila, columna;
        Scanner entrada= new Scanner(System.in);
        System.out.println("Ingrese la cantidad de filas de la matriz: ");
        fila= entrada.nextInt();
        System.out.println("Ingrese la cantidad de columnas de la matriz");
        columna= entrada.nextInt();
        int matrizSuma[][]= new int[fila][columna];
        int sumaMatriz;
        for(int i = 0; i < matrizSuma.length; i++) {
            for(int j = 0; j < matrizSuma[i].length; j++) {
                matrizSuma[i][j]= (i + j) * 2;
            }
        }

    }

    public static int sumarMatriz(int array[][]) {
        int suma= 0;
        for(int i = 0; i < array.length; i++) {
            for(int j = 0; j < array[i].length; j++) {
                suma += array[i][j];
            }
        }
        return suma;
    }

    public static void printMatriz(int array[][]) {
        for(int i = 0; i < array.length; i++) {
            System.out.println(java.util.Arrays.toString(array[i]));
        }
    }
}