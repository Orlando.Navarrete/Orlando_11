package ni.edu.uni.programacion1.pojo;

public class Producto {
    private int existingAmount;
    private float actualPrice;
    private String nameProduct;

    public Producto() {
    }

    public Producto(int existingAmount, float actualPrice, String nameProduct) {
        this.existingAmount = existingAmount;
        this.actualPrice = actualPrice;
        this.nameProduct = nameProduct;
    }

    public int getExistingAmount() {
        return existingAmount;
    }

    public void setExistingAmount(int existingAmount) {
        this.existingAmount = existingAmount;
    }

    public float getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(float actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }
    
}
