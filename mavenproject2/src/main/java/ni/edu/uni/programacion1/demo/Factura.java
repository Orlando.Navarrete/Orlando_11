package ni.edu.uni.programacion1.demo;

import ni.edu.uni.programacion1.pojo.Empleado;
import java.util.ArrayList;
import java.util.Scanner;
import ni.edu.uni.programacion1.pojo.Cliente;

public class Factura {
    ArrayList<Empleado> empleados= new ArrayList<>();
    ArrayList<Cliente> clientes= new ArrayList<>();

    public static void main(String[] args) {
        Scanner entrada= new Scanner(System.in);
        Factura facturas= new Factura();
        facturas.empleados= new ArrayList();
        facturas.clientes= new ArrayList();
        //ArrayList<Factura> facturas= new ArrayList<>();
        System.out.print("Digite una cantidad: ");
        int n= entrada.nextInt();
        for(int i = 0; i < n; i++) {
            System.out.println("Posicion ["+(i+1)+"].");
            System.out.println("Ingrese No. Empleado y No. Cliente: ");
            System.out.print("Empleado: "); int numeroEmpleado= entrada.nextInt();
            System.out.print("Cliente: "); int numeroCliente= entrada.nextInt();
            entrada.nextLine();
            System.out.println("Ingrese nombre del empleado y del cliente: ");
            System.out.print("Empleado: "); String nombreEmpleado= entrada.nextLine();
            System.out.print("Cliente: "); String nombreCliente= entrada.nextLine();
            System.out.println("Ingrese apellido del empleado y del cliente: ");
            System.out.print("Empleado: "); String apellidoEmpleado= entrada.nextLine();
            System.out.print("Cliente: "); String apellidoCliente= entrada.nextLine();
            System.out.println("Ingrese fecha de nacimiento del empleado y  del cliente: ");
            System.out.print("Empleado: "); String fechaNacimientoEmpleado= entrada.nextLine();
            System.out.print("Cliente: "); String fechaNacimientoCliente= entrada.nextLine();
            System.out.println("Ingrese direccion del empleado y del cliente: ");
            System.out.print("Empleado: "); String direccionEmpleado= entrada.nextLine();
            System.out.print("Cliente: "); String direccionCliente= entrada.nextLine();
            System.out.println("Ingrese telefono del empleado y del cliente: ");
            System.out.print("Empleado: "); String telefonoEmpleado= entrada.nextLine();
            System.out.print("Cliente: "); String telefonoCliente= entrada.nextLine();
            facturas.empleados.add(new Empleado(numeroEmpleado, nombreEmpleado, apellidoEmpleado, fechaNacimientoEmpleado, direccionEmpleado, telefonoEmpleado));
            facturas.clientes.add(new Cliente(numeroCliente, nombreCliente, apellidoCliente, fechaNacimientoCliente, direccionCliente, telefonoCliente));
            System.out.println();
        }
        for(Empleado e: facturas.empleados) {
            consultaEmpleado(e);
            System.out.println("============");
        }
        System.out.println();
        for(Cliente c: facturas.clientes) {
            consultaCliente(c);
            System.out.println("============");
        }
    }
    
    public static void consultaEmpleado(Empleado e) {
        System.out.println("No Empleado: "+e.getNoEmpleado());
        System.out.println("Nombre: "+e.getNombre());
        System.out.println("Apellido: "+e.getApellido());    
    }
    
    public static void consultaCliente(Cliente c) {
        System.out.println("No. Cliente: "+c.getNoCliente());
        System.out.println("Nombre: "+c.getNombreCliente());
        System.out.println("Apellido: "+c.getApellidoCliente());
    }
    
}