package ni.edu.uni.programacion1.demo;

import static java.lang.Thread.sleep;
import java.util.Arrays;
import java.util.Scanner;
import ni.edu.uni.programacion1.pojo.Producto;

public class ApplicationProducto {

    public static void main(String[] args) throws InterruptedException {
        Scanner entrada= new Scanner(System.in);
        Producto productos[];
        productos = null;
        int op, amount, aumentador= 0;
        float price, recaudacion= 0;
        String name;
        entrada.nextLine();
        do {
            printMenuOpciones();
            op= entrada.nextInt();
            System.out.println();
            switch(op) {
                case 1:
                    entrada.nextLine();
                    System.out.print("Ingrese nombre del producto: ");
                    name= entrada.nextLine();
                    System.out.print("Ingrese precio del producto: ");
                    price= entrada.nextFloat();
                    System.out.print("Ingrese canidad del producto: ");
                    amount= entrada.nextInt();
                    productos= addProducto(productos, amount, price, name);
                    break;
                case 2:
                    for(Producto e: productos) {
                        printObject(e);
                        System.out.println("==========================");
                    }
                    break;
                case 3:
                    System.out.println("Tenga en cuenta que el primer producto es '0', el segundo '1' y asi sucesivamente...");
                    System.out.print("Ingrese codigo del producto que desea comprar: ");
                    int cod= entrada.nextInt();
                    for(Producto e: productos) {
                        if(aumentador == cod) {
                            System.out.print("Ingrese la cantidad que desea comprar: ");
                            int cantCompra= entrada.nextInt();
                            float precio= e.getActualPrice();
                            int cantidad= e.getExistingAmount();
                            String nameBuy= e.getNameProduct();
                            if(cantCompra > cantidad) {
                                do {
                                    System.out.println("La cantidad debe ser igual o menor a la existente.");
                                    System.out.print("Ingrese nuevamente la cantidad que desea comprar: ");
                                    cantCompra= entrada.nextInt();
                                }while(cantCompra > cantidad);
                            }
                            int compraObject= cantidad - cantCompra;
                            float total= cantCompra * precio;
                            System.out.println("Total a pagar: "+total);
                            recaudacion += cantCompra * precio;
                            productos[cod]= new Producto(compraObject, precio, nameBuy);
                        }
                        aumentador++;
                    }
                    aumentador= 0;
                    break;
                case 4:
                    int j= 0;
                    for(Producto p: productos) {
                        System.out.println("La cantidad existente del producto("+j+") es: "+p.getExistingAmount());
                        j++;
                    }
                    break;
                case 5:
                    System.out.println("La recaudacion total es: "+recaudacion);
                    break;
                case 6:
                    System.out.println("Saliendo del programa...");
                    sleep(2000);
                    break;
                default:
                    System.out.println("Opcion incorrecta...");
                break;
            } 
        }while(op != 6);
    }
    
    public static void printMenuOpciones() {
        System.out.println("***Menu de opciones***");
        System.out.println("Opcion 1: Ingresar datos");
        System.out.println("Opcion 2: Visualizar datos de producto");
        System.out.println("Opcion 3: Realizar venta");
        System.out.println("Opcion 4: Consultar inventario");
        System.out.println("Opcion 5: Consultar recaudacion total");
        System.out.println("Opcion 6: Salir");
        System.out.println(); System.out.print("Opcion (1 - 6): ");
    }
    
    public static void printObject(Producto e) {
        System.out.println("Nombre: "+e.getNameProduct());
        System.out.println("Cantidad: "+e.getExistingAmount());
        System.out.println("Precio: "+e.getActualPrice());
    }
    
    public static Producto[] addProducto(Producto productos[], int cant, float precio, String nombre) {
        if(productos == null) {
            productos= new Producto[1];
            productos[productos.length - 1]= new Producto(cant, precio, nombre);
            return productos;
        }
        
        productos= Arrays.copyOf(productos, productos.length + 1);
        productos[productos.length - 1]= new Producto(cant, precio, nombre);
        
        return productos;
    }
}
